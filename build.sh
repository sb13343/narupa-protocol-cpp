git clone https://gitlab.com/intangiblerealities/narupa-protocol
out_path=./narupa-cpp
proto_include_path=./narupa-protocol/protocol
protoc_path=grpc/cmake/install/bin/ #protoc compiler binary
ls $protoc_path # debug to check protoc exists.
grpc_cpp_plugin=$protoc_path/grpc_cpp_plugin # where grpc cpp plugin is
mkdir -p $out_path
PROTO_FILES=$(find $proto_include_path -type f -name '*.proto') # grab all the files.
for file in $PROTO_FILES 
do
    $protoc_path/protoc -I$proto_include_path --cpp_out=$out_path --grpc_out=$out_path --plugin=protoc-gen-grpc=$grpc_cpp_plugin $file;
done

